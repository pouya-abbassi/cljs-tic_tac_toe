(ns cljs-tic_tac_toe.core
    (:require [reagent.core :as reagent :refer [atom]]))

(enable-console-print!)

;; define your app data so that it doesn't get over-written on reload

(defonce game-state (atom {:player \X}))

(defn click [element]
  "Change :player on each click. Let's assume the click is always valid."
  (swap! game-state 
         #(if (= (:player %) \X)
            (assoc % :player \O)
            (assoc % :player \X)))
)

(defn box [text]
  [:button {:class "box" :on-click click} nil]
)

(defn board []
  [:div {:id "board"}
   [:div {:class "row"}
    [box 0] [box 1] [box 2]]
   [:div {:class "row"}
    [box 3] [box 4] [box 5]]
   [:div {:class "row"}
    [box 6] [box 7] [box 8]]]
)

(defn game []
  [:div
   [:h1 "Current player: "
    [:small {:id "current-player"} (:player @game-state)]]
   [board]])

(reagent/render-component [game]
                          (. js/document (getElementById "app")))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)

